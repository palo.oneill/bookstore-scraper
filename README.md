# Bookstore Scraper

The Bookstore Scraper Service is a Python service that is capable of retrieving book data from various online bookstores around the world. The service covers several countries, and each country can have one or more associated bookstores. The service is easily extendable to include new countries and bookstores without the need for modifying the existing code.

By default, the service will scrape book information from online bookstores for each country where a parser has been implemented. To add a new parser, create a new folder in the "country_parsers" directory with the alpha-2 code for the country according to [ISO 3166](https://www.iso.org/obp/ui/) as its name. Then, create a new parser file in this folder with the postfix "\_parser.py" in the filename.

The Bookstore Scraper Service includes a functional parser for Pantarhei, one of the biggest online bookstores in Slovakia. This parser retrieves book titles along with their authors and prices. The output is displayed in JSON format in the console.

In the "utils" folder, auxiliary functions and classes can be added later, which can be used in other parts of the project.

## Features

-   Easily extendable to cover new countries and bookstores
-   One functional parser for online bookstores in Slovakia
-   Outputs book information in JSON format to the console

## Requirements

-   🐋 Docker

## Getting Started

To get started, clone the repository and build the Docker image:

```bash
git clone https://github.com/palo.oneill/bookstore-scraper.git
cd bookstore-scraper
docker build -t bookstore-scraper .
```

Then, run the service container:

`docker run --rm bookstore-scraper`

By default, the scraper will retrieve book data from a single page of each bookstore. To specify the number of pages to scrape, pass the num_pages parameter to the scrape_bookstore function in the instance of BookstoreScraper class defined in app.py. For example, to scrape two pages, you would call:

```py
scraper = BookstoreScraper()
scraper.scrape_bookstore(num_pages=2)
```

## Testing 🧪

To run the tests, use the following command:

`docker run --rm -e TESTING=true bookstore-scraper`

This will execute all the tests in the `tests` folder.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
