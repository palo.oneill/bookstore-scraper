from bookstore_scraper import BookstoreScraper

if __name__ == "__main__":
    scraper = BookstoreScraper()
    results = scraper.scrape_bookstore(num_pages=2)
    print(results)
