import requests
from bs4 import BeautifulSoup
from requests.exceptions import HTTPError


class BookstoreParser:
    """
    Class that allows parsing books from a specified bookstore URL.

    Attributes:
    url (str): URL of the bookstore to parse from.
    page_attr (str): The URL parameter represents the key for the page number.

    Methods:
    parse(num_pages=1): Extracts book information from the specified bookstore URL and returns a list of books.
    get_soup(url): Returns a BeautifulSoup object representing the HTML content of a specified URL.
    extract_books(soup): Extracts book information from a BeautifulSoup object and returns a list of books.

    """

    def __init__(self):
        """
        Initializes a BookstoreParser object.

        Attributes:
        url (str): URL of the bookstore to parse from.
        page_attr (str): The URL parameter represents the key for the page number.
        """
        self.url = None
        self.page_attr = None

    def parse(self, num_pages: int = 1) -> list:
        """
        Extracts book information from the specified bookstore URL and returns a list of books.

        Args:
        num_pages (int, optional): Number of pages to extract book information from. Default is 1.

        Returns:
        list: A list of dictionaries representing books with keys 'title', 'author', and 'price'.
        """
        books = []
        for page in range(1, num_pages + 1):
            page_url = (
                self.url + f"&{self.page_attr}={page}"
                if "?" in self.url
                else self.url + f"?{self.page_attr}={page}"
            )
            soup = self.get_soup(page_url)
            books.extend(self.extract_books(soup))
        return books

    def get_soup(self, url: str) -> BeautifulSoup:
        """
        Fetches the content of the given URL and returns a BeautifulSoup object
        with the HTML content of the response.

        Args:
            url (str): The URL to fetch.

        Returns:
            BeautifulSoup: The parsed HTML content of the response.

        Raises:
            HTTPError: If the request to the URL fails with a HTTP error status code.
            Exception: If any other error occurs during the request or parsing.
        """
        response = requests.get(url, timeout=5)
        try:
            response.raise_for_status()
        except HTTPError as http_err:
            print(f"HTTP error occurred: {http_err}")
            raise
        except Exception as err:
            print(f"Other error occurred: {err}")
            raise
        soup = BeautifulSoup(response.content, "html.parser")
        return soup

    def extract_books(self, soup: BeautifulSoup) -> list:
        """
        Extracts book information from a BeautifulSoup object and returns a list of books.

        Args:
        soup (BeautifulSoup object): A BeautifulSoup object representing the HTML content of a bookstore URL.

        Returns:
        list: A list of dictionaries representing books with keys 'title', 'author', and 'price'.

        Raises:
            NotImplementedError: Implemented in subclasses.
        """
        raise NotImplementedError("Please Implement this method")
