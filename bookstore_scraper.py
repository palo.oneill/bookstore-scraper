import os
import importlib.util
import json
from parsers.bookstore_parser import BookstoreParser


class BookstoreScraper:
    """
    A class to scrape bookstore data from different countries using various parsers.

    Attributes:
    - bookstore_data (dict): a dictionary that holds the scraped bookstore data
    - country_parsers (dict): a dictionary that holds the parsers for each country

    Methods:
    - load_country_parsers: loads the parsers for each country
    - scrape_bookstore: scrapes the bookstore data for each country and returns a JSON string
    """

    def __init__(self):
        """
        Initializes the `BookstoreScraper` object by creating an empty dictionary for `bookstore_data`
        and calling the `load_country_parsers` method to load the parsers for each country.

        """
        self.bookstore_data = {}
        self.load_country_parsers()

    def load_country_parsers(self) -> None:
        """
        Loads the parser for each country and adds it to the `country_parsers` dictionary.
        The `country_parsers` dictionary is structured as follows:
        {
            "country1": [parser1, parser2, ...],
            "country2": [parser3, parser4, ...],
            ...
        }
        """
        self.country_parsers = {}
        country_dirs = os.listdir("country_parsers")
        for country_dir in country_dirs:
            if (
                os.path.isdir(f"country_parsers/{country_dir}")
                and not country_dir.startswith("__")
                and not country_dir.endswith("__")
            ):
                self.country_parsers[country_dir] = []
                parser_files = os.listdir(f"country_parsers/{country_dir}")
                for parser_file in parser_files:
                    if parser_file.endswith("_parser.py"):
                        parser_module = (
                            f"country_parsers.{country_dir}.{parser_file[:-3]}"
                        )
                        spec = importlib.util.spec_from_file_location(
                            parser_module,
                            f"country_parsers/{country_dir}/{parser_file}",
                        )
                        module = importlib.util.module_from_spec(spec)
                        spec.loader.exec_module(module)
                        for _, obj in module.__dict__.items():
                            if (
                                isinstance(obj, type)
                                and issubclass(obj, BookstoreParser)
                                and obj is not BookstoreParser
                            ):
                                self.country_parsers[country_dir].append(obj())

    def scrape_bookstore(self, num_pages: int = 1) -> str:
        """
        Scrapes the bookstore data for each country and returns a JSON string.

        Args:
        - num_pages (int): the number of pages to scrape for each country (default: 1)

        Returns:
        - A JSON string that contains the scraped bookstore data for each country. The `bookstore_data`
          dictionary is structured as follows:
          {
              "country1": [
                  {
                      "name": "parser1",
                      "books": [
                          {
                              "title": "book1",
                              "author": "author1",
                              "price": "price1",
                          },
                          ...
                      ]
                  },
                  ...
              ],
              "country2": [
                  ...
              ],
              ...
          }
        """
        for country, parsers in self.country_parsers.items():
            country_data = []
            for parser in parsers:
                bookstore_data = {}
                bookstore_data["name"] = (
                    type(parser).__name__.replace("BookstoreParser", "").lower()
                )
                bookstore_data["books"] = parser.parse(num_pages=num_pages)
                country_data.append(bookstore_data)
            self.bookstore_data[country] = country_data
        return json.dumps(self.bookstore_data, indent=4, ensure_ascii=False)
