from bs4 import BeautifulSoup
from parsers.bookstore_parser import BookstoreParser


class PantarheiBookstoreParser(BookstoreParser):
    """
    Class that allows parsing books from a specified bookstore URL.

    Attributes:
    url (str): URL of the bookstore to parse from.
    page_attr (str): The URL parameter represents the key for the page number.

    Methods:
    parse(pages=1): Extracts book information from the specified bookstore URL and returns a list of books.
    get_soup(url): Returns a BeautifulSoup object representing the HTML content of a specified URL.
    extract_books(soup): Extracts book information from a BeautifulSoup object and returns a list of books.

    """

    def __init__(self):
        """
        Initializes a BookstoreParser object.

        Args:
        url (str): URL of the bookstore to parse from.
        page_attr (str): The URL parameter represents the key for the page number.
        """
        super().__init__()
        self.url = "https://www.pantarhei.sk/knihy/beletria"
        self.page_attr = "p"

    def extract_books(self, soup: BeautifulSoup) -> list:
        """
        Extracts book information from a BeautifulSoup object and returns a list of books.

        Args:
        soup (BeautifulSoup object): A BeautifulSoup object representing the HTML content of a bookstore URL.

        Returns:
        list: A list of dictionaries representing books with keys 'title', 'author', and 'price'.

        Raises:
        ValueError: If the soup object is None.
        """
        if soup is None:
            raise ValueError("Soup object cannot be None.")

        books = []
        # start implement parsing logic
        book_list_wrapper = soup.find(
            "div", {"class": "row no-gutters product-list products wrapper"}
        )
        if book_list_wrapper:
            book_list = book_list_wrapper.find_all(
                "div", {"class": "row px-md-2 p-lg-2"}
            )
            for book in book_list:
                title_element = book.find("a", {"class": "title"})
                if title_element is None or title_element.span is None:
                    continue
                title = title_element.span.text.strip()

                author_elements = book.find_all("a", {"class": "author d-inline"})
                author = " ".join(
                    [b_author.text.strip() for b_author in author_elements]
                )

                price_element = book.find("span", {"class": "price"})
                if price_element is None:
                    price = ""
                elif price_element.span is not None:
                    price = price_element.span.span.text.strip()
                else:
                    price = price_element.text.strip()

                books.append({"title": title, "author": author.strip(), "price": price})
        # end implement parsing logic

        return books
