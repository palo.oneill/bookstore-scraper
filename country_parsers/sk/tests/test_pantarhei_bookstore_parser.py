import unittest
from bs4 import BeautifulSoup
from country_parsers.sk.pantarhei_bookstore_parser import PantarheiBookstoreParser


class TestPantarheiBookstoreParser(unittest.TestCase):
    """
    This class contains unit tests for the PantarheiBookstoreParser class in the
    country_parsers.slovakia module. It tests the extract_books() method of the
    PantarheiBookstoreParser class with different input data.

    Each test method tests the function with a different input scenario:
    - test_extract_books_with_valid_data: tests the function with a valid BeautifulSoup
      object containing book data.
    - test_extract_books_with_empty_data: tests the function with an empty BeautifulSoup
      object.
    - test_extract_books_with_invalid_data: tests the function with a BeautifulSoup
      object missing some required elements.

    The expected output of each test is predefined, and it is compared with the output
    of the function being tested using the assertEqual() method.

    To run the tests, run this module as a script.
    """

    def setUp(self):
        self.parser = PantarheiBookstoreParser()

    def test_extract_books_with_valid_data(self) -> None:
        """
        Test the function with a valid BeautifulSoup object containing book data.
        """
        # test data
        html = (
            '<html><body><div class="row no-gutters product-list products wrapper">'
            '<div class="row px-md-2 p-lg-2">'
            '<a class="title"><span>Title 1</span></a><a class="author d-inline">Author 1</a>'
            '<span class="price"><span><span>9.99 €</span></span></span>'
            '</div><div class="row px-md-2 p-lg-2">'
            '<a class="title"><span>Title 2</span></a><a class="author d-inline">Author 2</a>'
            '<span class="price"><span><span>14.99 €</span></span></span>'
            "</div></div></body></html>"
        )
        soup = BeautifulSoup(html, "html.parser")
        expected_output = [
            {"title": "Title 1", "author": "Author 1", "price": "9.99 €"},
            {"title": "Title 2", "author": "Author 2", "price": "14.99 €"},
        ]
        self.assertEqual(self.parser.extract_books(soup), expected_output)

    def test_extract_books_with_empty_data(self) -> None:
        """
        Test the function with an empty BeautifulSoup object.
        """
        # test data
        html = "<html><body></body></html>"
        soup = BeautifulSoup(html, "html.parser")
        expected_output = []
        self.assertEqual(self.parser.extract_books(soup), expected_output)

    def test_extract_books_with_invalid_data(self) -> None:
        """
        Test the function with a BeautifulSoup object missing some required elements.
        """
        # test data
        html = (
            '<html><body><div class="row no-gutters product-list products wrapper">'
            '<div class="row px-md-2 p-lg-2">'
            '<span class="price"><span><span>9.99 €</span></span></span>'
            "</div></div></body></html>"
        )
        soup = BeautifulSoup(html, "html.parser")
        expected_output = []
        self.assertEqual(self.parser.extract_books(soup), expected_output)


if __name__ == "__main__":
    unittest.main()
